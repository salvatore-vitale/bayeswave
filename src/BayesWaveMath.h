/* ********************************************************************************** */
/*                                                                                    */
/*                                    Math tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

double gaussian_norm(double min, double max, double sigma);

void recursive_phase_evolution(double dre, double dim, double *cosPhase, double *sinPhase);

double network_nwip(int imin, int imax, double **a, double **b, double **invSn, double *eta, int NI);
double network_snr(int imin, int imax, double **h, double **invpsd, double *eta, int NI );
double detector_snr(int imin, int imax, double *g, double *invpsd, double eta);
double fourier_nwip(int imin, int imax, double *a, double *b, double *invSn);

/* ***********************************************************************************/
/*                   																					                       */
/*                                  Matrix Routines                                  */
/*											                                                             */
/* ***********************************************************************************/

void matrix_eigenstuff(double **matrix, double **evector, double *evalue, int N);
double matrix_jacobian(double **matrix, int N);
void matrix_multiply(double **matrix, double *vector, double *result, int N);
double dot_product(double *a, double *b, int N);

/* ********************************************************************************** */
/*																					                                          */
/*                                 Fourier Routines                                   */
/*											                                                              */
/* ********************************************************************************** */

void dfour1(double data[], unsigned long nn, int isign);
void drealft(double data[], unsigned long n, int isign);
void Fourier_Scaling(double *ft, int n, double Tobs);

/* ********************************************************************************** */
/*																					                                          */
/*                                       RNGS                                         */
/*											                                                              */
/* ********************************************************************************** */

double gasdev2(long *idum);
double ran2(long *idum);
