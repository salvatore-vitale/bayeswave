#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                             Instrument noise routines                              */
/*                                                                                    */
/* ********************************************************************************** */

void Shf_Geocenter(struct Data *data, struct Model *model, double *SnGeo, double *params);
void Shf_Geocenter_full(struct Data *data, struct Network *projection, double **Snf, double *SnGeo, double *params);

void OverlapReductionFunction(struct Data *data);
void ComputeNoiseCorrelationMatrix(struct Data *data, double **Snf, double *Sn, struct Background *background);

double symmetric_snr_ratio(struct Data *data, struct Network *projection, double *params);

/* ********************************************************************************** */
/*                                                                                    */
/*                           Memory (de)allocation routines                           */
/*                                                                                    */
/* ********************************************************************************** */

void initialize_fisher(struct FisherMatrix *fisher, int N);
void free_fisher(struct FisherMatrix *fisher);

void free_chain        (struct Chain *chain, int NI, int NP);
void allocate_chain    (struct Chain *chain, int NI, int NP);
void initialize_chain  (struct Chain *chain, int flag);
void resize_model      (struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model, struct BayesLineParams ***bayesline, double **psd, int NC);

void reset_priors      (struct Data *data, struct Prior *prior);
void initialize_priors (struct Data *data, struct Prior *prior, int omax);

void initialize_data   (struct Data *data, double **s, int N, int tsize, double Tobs, int NI, double fmin, double fmax);

void reset_model       (struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model);
void initialize_model  (struct Model *model, struct Data *data, struct Prior *prior, double **psd, long *seed);
void free_model        (struct Model *model, struct Data *data, struct Prior *prior);
void copy_int_model    (struct Model *origin, struct Model *copy, int N, int NI, int det);
void copy_ext_model    (struct Model *origin, struct Model *copy, int N, int NI);
void copy_psd_model    (struct Model *origin, struct Model *copy, int N, int NI);

void free_bayesline(struct BayesLineParams **bayesline, struct Data *data);
void initialize_bayesline(struct BayesLineParams **bayesline, struct Data *data, double **psd, int nstep);

void reset_likelihood(struct Data *data);
void reset_params(struct Wavelet *wave_x, struct Wavelet *wave_y, double **range);

void initialize_wavelet(struct Wavelet *wave, int N, int smax, int size);
void free_wavelet      (struct Wavelet *wave, int N, int smax);
void copy_wavelet      (struct Wavelet *origin, struct Wavelet *copy, int N);

void initialize_background(struct Background *background, int NI, int N);
void copy_background      (struct Background *origin, struct Background *copy, int NI, int N);
void free_background      (struct Background *background, int NI, int N);

void initialize_network(struct Network *projection, int N, int NI);
void free_network(struct Network *projection, int NI);

void initialize_TF_proposal(struct Data *data, struct Prior *prior, struct TimeFrequencyMap *tf);
void free_TF_proposal(struct Data *data, struct TimeFrequencyMap *tf);

int checkfile(char *name);

int *ivector(long nl, long nh);
int **imatrix(long nrl, long nrh, long ncl, long nch);
int ***i3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);

unsigned long *lvector(long nl, long nh);

double *dvector(long nl, long nh);
double **dmatrix(long nrl, long nrh, long ncl, long nch);
double ***d3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);

void free_ivector(int *v, long nl, UNUSED long nh);
void free_dvector(double *v, long nl, UNUSED long nh);
void free_lvector(unsigned long *v, long nl, UNUSED long nh);
void free_imatrix(int **m, long nrl, UNUSED long nrh, long ncl, UNUSED long nch);
void free_dmatrix(double **m, long nrl, UNUSED long nrh, long ncl, UNUSED long nch);
void free_i3tensor(int ***t, long nrl, UNUSED long nrh, long ncl, UNUSED long nch, long ndl, UNUSED long ndh);
void free_d3tensor(double ***t, long nrl, UNUSED long nrh, long ncl, UNUSED long nch, long ndl, UNUSED long ndh);

