#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <lal/LALSimSGWB.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveIO.h"
#include "BayesWaveModel.h"
#include "BayesWavePrior.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveLikelihood.h"
#include "BayesWaveGlitchPrior.h"

#define NR_END 1
#define FREE_ARG char*

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif


/* ********************************************************************************** */
/*                                                                                    */
/*                             Instrument noise routines                              */
/*                                                                                    */
/* ********************************************************************************** */


void Shf_Geocenter(struct Data *data, struct Model *model, double *SnGeo, double *params)
{
    int ifo,i,n;
    int NI = data->NI;

    struct Network *projection = model->projection;
    double **Snf = model->Snf;

    double AntennaPattern;
    double ecc = params[3];

    int nmax = model->signal[0]->size;
    int index[model->signal[0]->size];

    for(n=0; n<nmax; n++)
    {
        i = (int)(model->signal[0]->intParams[n+1][1]*data->Tobs);
        index[n] = i;
        SnGeo[i] = 0.0;
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      //AntennaPattern = 1.0;
    AntennaPattern = 0.1; /* averaged over prior of all extrinsic parameters */
        if(data->geocenterPSDFlag) AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
        for(n=0; n<nmax; n++)
        {
            i = index[n];
            SnGeo[i] += AntennaPattern/(Snf[ifo][i]);
        }
    }

    for(n=0; n<nmax; n++)
    {
        i = index[n];
        SnGeo[i] = 1./SnGeo[i];
    }
    
}

void Shf_Geocenter_full(struct Data *data, struct Network *projection, double **Snf, double *SnGeo, double *params)
{
  int ifo,i, halfN=data->N/2;
  int NI = data->NI;

  int imin = data->imin;
  int imax = data->imax;

  double AntennaPattern;
  double ecc = 1.0;
  if(data->polarizationFlag)ecc=params[3];

  for(i=0; i<halfN; i++) SnGeo[i] = 0.0;

  for(ifo=0; ifo<NI; ifo++)
  {
      //AntennaPattern = 1.0;
    AntennaPattern = 0.1; /* averaged over prior of all extrinsic parameters */
      if(data->geocenterPSDFlag) AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
    for(i=imin; i<imax; i++) SnGeo[i] += AntennaPattern/(Snf[ifo][i]);
  }

  for(i=0; i<imin; i++)SnGeo[i]=1.0;
  for(i=imax; i<halfN; i++)SnGeo[i]=1.0;
  for(i=imin; i<imax; i++)  SnGeo[i] = 1./SnGeo[i];
}

void OverlapReductionFunction(struct Data *data)
{

  int i;
  double f;
  double invTobs = 1.0/data->Tobs;

  if(data->NI==2)
  {
    fprintf(stdout,"Calculating overlap reduction function, NI=%i\n\n",data->NI);
    for(i=0; i<data->N/2; i++)
    {

      f = (double)(i)*invTobs;
      data->ORF[i] = XLALSimSGWBOverlapReductionFunction(f, data->detector[0], data->detector[1]);
    }
  }
  else
  {
    fprintf(stdout,"Skipping overlap reduction function, NI=%i\n\n",data->NI);
  }
}

void ComputeNoiseCorrelationMatrix(struct Data *data, double **Snf, double *Sn, struct Background *background)
{
  int i;

  double C11, C12, C21, C22;
  double x;
  double invC;
  double invTobs = 1.0/data->Tobs;
  double invTf   = invTobs*background->invfref;
  double amp     = exp(background->logamp);

  for(i=data->imin; i<data->imax; i++)
  {

    /*
     Calculate network response to stochastic background parameters
     */
    x = (double)i*invTf;

    background->spectrum[i] = amp * powf(x,background->index);

    /*
     Calculate noise correlation matrix
     Combining instrument noise and stochastic backgrouns signal
     */
    C11 = background->spectrum[i] + Snf[0][i]*Sn[0];
    C22 = background->spectrum[i] + Snf[1][i]*Sn[1];
    C12 = background->spectrum[i] * data->ORF[i];
    C21 = C12;

    background->detCij[i] = C11*C22 - C12*C21;
    invC = 1./background->detCij[i];

    background->Cij[0][0][i] =  C22*invC;
    background->Cij[0][1][i] = -C12*invC;
    background->Cij[1][0][i] = -C21*invC;
    background->Cij[1][1][i] =  C11*invC;

//    printf("%lg %lg\n%lg %lg\n",background->Cij[0][0][i],background->Cij[0][1][i],background->Cij[1][0][i],background->Cij[1][1][i]);
//    printf("================\n");
//    printf("%lg %lg\n",Snf[0][i]*Sn[0],data->Snf[1][i]*Sn[1]);
  }
}

double symmetric_snr_ratio(struct Data *data, struct Network *projection, double *params)
{
   int ifo;
   int NI = data->NI;

   double AntennaPattern;
   double ecc = params[3];

   double SNR[NI];

   for(ifo=0; ifo<NI; ifo++)
   {
      AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
      SNR[ifo] = AntennaPattern;
   }

   return SNR[0]*SNR[1]/(SNR[0]+SNR[1])/(SNR[0]+SNR[1]);
   
}

/* ********************************************************************************** */
/*                                                                                    */
/*                           Memory (de)allocation routines                           */
/*                                                                                    */
/* ********************************************************************************** */


void initialize_fisher(struct FisherMatrix *fisher, int N)
{
  int i;
  fisher->N = N;

  fisher->aamps	    = dvector(0,fisher->N-1);
  fisher->sigma	    = dvector(0,fisher->N-1);
  fisher->evalue  	= dvector(0,fisher->N-1);

  fisher->count     = dmatrix(0,fisher->N-1,0,fisher->N-1);
  fisher->matrix	  = dmatrix(0,fisher->N-1,0,fisher->N-1);
  fisher->evector	  = dmatrix(0,fisher->N-1,0,fisher->N-1);

  for(i=0;i<fisher->N;i++)
  {
    fisher->aamps[i] = 1.0;
    fisher->count[0][i] = 1;
    fisher->count[1][i] = 1;
  }
}

void free_fisher(struct FisherMatrix *fisher)
{

  free_dvector(fisher->aamps  ,0,fisher->N-1);
  free_dvector(fisher->sigma  ,0,fisher->N-1);
  free_dvector(fisher->evalue ,0,fisher->N-1);

  free_dmatrix(fisher->count  ,0,fisher->N-1,0,fisher->N-1);
  free_dmatrix(fisher->matrix ,0,fisher->N-1,0,fisher->N-1);
  free_dmatrix(fisher->evector,0,fisher->N-1,0,fisher->N-1);
}


void initialize_chain(struct Chain *chain, int flag)
{
  chain->mod0     = 0;     //Counter keeping track of noise model
  chain->mod1     = 0;     //Counter keeping track of signal model
  chain->mod2     = 0;     //Counter keeping track of noise model
  chain->mod3     = 0;     //Counter keeping track of signal model
  chain->mcount   = 1;     //# of MCMC proposals
  chain->scount   = 1;     //# of RJMCMC proposals
  chain->xcount   = 1;     //# of extrinsic proposals
  chain->fcount   = 1;     //# of intrinsic fisher proposals
  chain->pcount   = 1;     //# of intrinsic phase proposals
  chain->ccount   = 1;     //# of cluster proposals
  chain->dcount   = 1;     //# of density proposals
  chain->ucount   = 1;     //# of uniform proposals
  chain->macc     = 0;     //# of MCMC successes
  chain->sacc     = 0;     //# of RJMCMC successes
  chain->xacc     = 0;     //# of extrinsic successes
  chain->facc     = 0;     //# of intrinsic fisher successes
  chain->pacc     = 0;     //# of intrinsic phase successes
  chain->cacc     = 0;     //# of cluster proposal successes
  chain->dacc     = 0;     //# of density proposal successes
  chain->uacc     = 0;     //# of uniform proposal successes
  chain->burnFlag = flag;  //Flag for burn-in (1 = yes)
}

void free_chain(struct Chain *chain, int NI, int NP)
{
   free(chain->ptacc);
   free(chain->ptprop);
   free(chain->index);
   free(chain->A);

   free(chain->dT);
   free(chain->temperature);
   free(chain->avgLogLikelihood);
   free(chain->varLogLikelihood);

   free(chain->intChainFile);

   int n;
   for(n=0; n<NP; n++) free(chain->intWaveChainFile[n]);
   free(chain->intWaveChainFile);

   for(n=0; n<NI; n++) free(chain->intGlitchChainFile[n]);
   free(chain->intGlitchChainFile);

  //logL chain for thermodynamic integration
  for(n=0; n<chain->NC; n++) free(chain->logLchain[n]);
  free(chain->logLchain);

}

void allocate_chain(struct Chain *chain, int NI, int NP)
{
   chain->ptacc        = malloc(chain->NC*sizeof(int));
   chain->ptprop       = malloc(chain->NC*sizeof(int));
   chain->index        = malloc(chain->NC*sizeof(int));
   chain->A            = malloc(chain->NC*sizeof(double));

   chain->dT               = malloc(chain->NC*sizeof(double));
   chain->temperature      = malloc(chain->NC*sizeof(double));
   chain->avgLogLikelihood = malloc(chain->NC*sizeof(double));
   chain->varLogLikelihood = malloc(chain->NC*sizeof(double));

   chain->intChainFile = malloc(chain->NC*sizeof(FILE*));

  int n;
  
   chain->intWaveChainFile = malloc(NP*sizeof(FILE**));
  for(n=0; n<NP; n++) chain->intWaveChainFile[n] = malloc(chain->NC*sizeof(FILE*));

   int ifo;
   chain->intGlitchChainFile = malloc(NI*sizeof(FILE**));
   for(ifo=0; ifo<NI; ifo++) chain->intGlitchChainFile[ifo] = malloc(chain->NC*sizeof(FILE*));

  int ic;
  chain->lineChainFile   = malloc(chain->NC*sizeof(FILE**));
  chain->splineChainFile = malloc(chain->NC*sizeof(FILE**));
  for(ic=0; ic<chain->NC; ic++)
  {
    chain->lineChainFile[ic]   = malloc(NI*sizeof(FILE*));
    chain->splineChainFile[ic] = malloc(NI*sizeof(FILE*));
  }

  //logL chain for thermodynamic integration
  int i;
  chain->nPoints = 3*chain->count/4/chain->cycle;
  chain->logLchain = malloc(chain->NC*sizeof(double *));
  for(i=0; i<chain->NC; i++)
  {
      chain->logLchain[i] = malloc(chain->nPoints*sizeof(double));
      for(n=0; n<chain->nPoints; n++) chain->logLchain[i][n]=0.0;
  }
}

void resize_model(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model, struct BayesLineParams ***bayesline, double **psd, int NC)
{
  int ic,ifo;

  fprintf(stdout, " Resizing model for number of chains from %i to %i\n",chain->NC,NC);

  struct BayesLineParams **bptr;
  bptr=malloc(data->NI*sizeof(struct BayesLineParams *));
  initialize_bayesline(bptr,data,psd,chain->blcount);
  for(ifo=0; ifo<data->NI; ifo++) copy_bayesline_params(bayesline[0][ifo], bptr[ifo]);


  //free model structure
  for(ic=0; ic<chain->NC; ic++)
  {
    free_bayesline(bayesline[ic], data);
    free(bayesline[ic]);

    free_model(model[ic], data, prior);
    free(model[ic]);
  }

  //choose new number of chains
  free_chain(chain, data->NI, data->Npol);
  chain->NC = NC;
  allocate_chain(chain, data->NI, data->Npol);
  for(ic=0; ic<chain->NC; ic++) chain->index[ic]=ic;

  *model      = realloc(*model,      chain->NC * sizeof(struct Model*)            );
  **bayesline = realloc(**bayesline, chain->NC * sizeof(struct BayesLineParams**) );

  for(ic=0; ic<chain->NC; ic++)
  {
    model[ic] = malloc(sizeof(struct Model));
    initialize_model(model[ic], data, prior, psd, &chain->seed);

    bayesline[ic] = malloc(data->NI*sizeof(struct BayesLineParams *));
    initialize_bayesline(bayesline[ic], data, psd, chain->blcount);
    for(ifo=0; ifo<data->NI; ifo++) copy_bayesline_params(bptr[ifo], bayesline[ic][ifo]);
  }

  free_bayesline(bptr, data);
  free(bptr);
}


void initialize_priors(struct Data *data, struct Prior *prior, int omax)
{
   int i,j,n;
   int NW = data->NW;
  
   /* Model dimension parameters */
   // number of sineGaussians we print to file
   prior->omax = omax;

   // maximum number of sineGaussians
   prior->gmin = ivector(0,data->NI-1);
   prior->gmax = ivector(0,data->NI-1);

    // wavelet density prior
    prior->Nwavelet = malloc(200*sizeof(double));
    double b = 2.9;
    //TODO Nwavelet unstable at i=0?
    for(i=1; i<200; i++) prior->Nwavelet[i] = log(((double)i*4.0*sqrt(3.)/(LAL_PI*pow(b,2.)))/(3.0+pow((double)i/b,4.)));
    prior->Nwavelet[0]=prior->Nwavelet[1];

   prior->smin = 0;
   prior->smax = data->Dmax;
   for(i=0; i<data->NI; i++)
   {
      prior->gmin[i] = 0;
      prior->gmax[i] = data->Dmax;
   }

   if(data->signalFlag || data->glitchFlag) prior->smin = 1;


   /* PSD paramters */
   prior->Snmin = 0.5;
   prior->Snmax = 2.0;


   /* Wavelet parameters */
   prior->range = dmatrix(0,NW-1,0,1);

   double Amin=0.0, Amax=1.0;
   //Set Amplitude priors based on the data channel being used
   //S6 strain
   if(!strcmp(data->channels[0],"LDAS-STRAIN"))
   {
      Amin = 5.e-25;
      Amax = 1.e-18;
   }

   //VSR2 strain
   else if (!strcmp(data->channels[0],"h_16384Hz"))
   {
      Amin = 5.e-24;
      Amax = 1.e-15;
   }

   //S6 LIGO DARM Error
   else if (!strcmp(data->channels[0],"LSC-DARM_ERR"))
   {
      Amin = 5.0e-9;
      Amax = 1.0e-3;
   }

   //Use S6 strain as default
   else
   {
     Amin = 4.78089288388547e-25;
     Amax = 7.7811322411338e-20;
   }

   //double dt   = data->Tobs/(double)data->tsize;
   double Qmin = data->Qmin;
   double Qmax = data->Qmax;
   double tmin = 0;//dt*(int)((data->Tobs/16.)/dt);
   double tmax = data->Tobs;//-tmin;
   double fmin = data->fmin;
   double fmax = data->fmax;
   double betamin = -1.0;
   double betamax = 1.0;

   /*
    if(data->amplitudePriorFlag)
    {
    Amin=0.0;
    Amax=1.0;
    }
    */

   prior->range[0][0] = tmin;
   prior->range[0][1] = tmax;
   prior->range[1][0] = fmin;
   prior->range[1][1] = fmax;
   prior->range[2][0] = Qmin;
   prior->range[2][1] = Qmax;
   prior->range[3][0] = Amin;
   prior->range[3][1] = Amax;
   prior->range[4][0] = 0.0;
   prior->range[4][1] = LAL_TWOPI;
    
    if(data->chirpletFlag)
    {
        prior->range[5][0] = betamin;
        prior->range[5][1] = betamax;
    }

   prior->TFV    = (tmax-tmin)*(fmax-fmin)*(Qmax-Qmin);
   prior->logTFV = log(prior->TFV);


   /**********************************/
   /* cluster prior                  */
   /**********************************/
   prior->bias = dvector(0,prior->smax);

  if(data->clusterPriorFlag)
  {
    
    FILE *normFile;
    char filename[1000];
    
    sprintf(filename,"%s/cluster_norm_flow%i_twin%.2f_srate%i_Qmax%i.dat",prior->path,(int)data->fmin,data->Twin,data->srate,(int)data->Qmax);
    printf("opening file %s\n",filename);
    if(!checkfile(filename))
    {
      fprintf(stdout,"\n");
      fprintf(stdout,"ERROR: Missing file to correct bias in clustering prior:\n");
      fprintf(stdout,"  path:   %s\n",filename);
      fprintf(stdout,"  flow:   %i\n",(int)data->fmin);
      fprintf(stdout,"  window: %i\n",(int)data->Twin);
      fprintf(stdout,"  srate:  %i\n",data->srate);
      fprintf(stdout,"  Qmax:   %i\n",(int)data->Qmax);
      fprintf(stdout,"TRY:\n");
      fprintf(stdout,"  Creating file to correct for bias\n");
      fprintf(stdout,"  Checking your --clusterPath\n");
      fprintf(stdout,"  Removing --clusterPrior\n\n");
      exit(1);
    }
    else
    {
      sprintf(filename,"%s/cluster_norm_flow%i_twin%.2f_srate%i_Qmax%i.dat",prior->path,(int)fmin,data->Twin,data->srate,(int)data->Qmax);
      normFile = fopen(filename,"r");
      
      for(i=0; i<prior->smax; i++)
      {
        fscanf(normFile,"%i %lg",&n,&prior->bias[i]);
        prior->bias[i] = log(prior->bias[i]);
      }
      
      fclose(normFile);
    }
  }
  
   /**********************************/
   /* background distribution prior  */
   /**********************************/
    if(data->backgroundPriorFlag)
    {
      printf("Initializing background glitch distribution prior:\n");
      //printf("   opening %s\n",prior->bkg_name);
      //FILE *bkg_file = fopen(prior->bkg_name,"r");
      //double f,Q,den;
      //char cmt;

      // Parse background density file

      //# 1024 400 2 0.1
      //fscanf(bkg_file,"%c %i %i %lg %lg",&cmt,&prior->bkg_nf,&prior->bkg_nQ,&prior->bkg_df,&prior->bkg_dQ);
      prior->bkg_nf=BKG_NF;
      prior->bkg_nQ=BKG_NQ;
      prior->bkg_df=BKG_DF;
      prior->bkg_dQ=BKG_DQ;

      prior->bkg_dist = dmatrix(0,prior->bkg_nf-1,0,prior->bkg_nQ-1);

      //for(n=0; n<prior->bkg_nf*prior->bkg_nQ; n++)
      for(i=0; i<prior->bkg_nf; i++)
      {
        for(j=0; j<prior->bkg_nQ; j++)
        {
          //fscanf(bkg_file,"%lg %lg %lg",&f,&Q,&den);
          //i = (int)round(f/prior->bkg_df);
          //j = (int)round(Q/prior->bkg_dQ);
          //prior->bkg_dist[i][j] = den;
          n=i*prior->bkg_nQ+j;
          prior->bkg_dist[i][j] = BCKG_DENSITY[n];
        }
      }
      //fclose(bkg_file);

      // Renormalize distribution to fit within f-Q prior
      int imin = (int)(fmin/prior->bkg_df);
      int imax = (int)(fmax/prior->bkg_df);
      int jmin = (int)(Qmin/prior->bkg_dQ);
      int jmax = (int)(Qmax/prior->bkg_dQ);

      double norm = 0.0;
      for(i=imin; i<imax; i++) for(j=jmin; j<jmax; j++) norm += prior->bkg_dist[i][j];
      for(i=imin; i<imax; i++) for(j=jmin; j<jmax; j++) prior->bkg_dist[i][j]/=(norm*prior->bkg_df*prior->bkg_dQ);
      for(i=imin; i<imax; i++) for(j=jmin; j<jmax; j++) prior->bkg_dist[i][j] = log(prior->bkg_dist[i][j]);

    }

}

void reset_priors(struct Data *data, struct Prior *prior)
{
   int i;

   prior->smin = 0;
   prior->smax = data->Dmax;
   for(i=0; i<data->NI; i++)
   {
      prior->gmin[i] = 0;
      prior->gmax[i] = data->Dmax;
   }

   if(data->signalFlag || data->glitchFlag) prior->smin = data->Dmin;

   /* PSD paramters */
   prior->Snmin = 0.5;
   prior->Snmax = 2.0;

   double Amin=0.0, Amax=1.0;
   //Set Amplitude priors based on the data channel being used
   //S6 strain
   if(!strcmp(data->channels[0],"LDAS-STRAIN"))
   {
      Amin = 5.e-25;
      Amax = 1.e-18;
   }

   //VSR2 strain
   else if (!strcmp(data->channels[0],"h_16384Hz"))
   {
      Amin = 5.e-24;
      Amax = 1.e-15;
   }

   //S6 LIGO DARM Error
   else if (!strcmp(data->channels[0],"LSC-DARM_ERR"))
   {
      Amin = 5.0e-9;
      Amax = 1.0e-3;
   }

   //Use S6 strain as default
   else
   {
    Amin = 4.78089288388547e-25;
    Amax = 7.7811322411338e-20;
   }

   double Qmin = data->Qmin;
   double Qmax = data->Qmax;
   double tmin = 0;//dt*(int)((data->Tobs/16.)/dt);
   double tmax = data->Tobs;//-tmin;
   double fmin = data->fmin;
   double fmax = data->fmax;
   double betamin = -1.0;
   double betamax = 1.0;
    
    
   //restrict prior range for wavelet time to 1 second around trigger
   if(data->runPhase==1)
   {
       tmin = data->Tobs/2.0-data->Twin/2.0;
       tmax = data->Tobs/2.0+data->Twin/2.0;
   }
   /*
    if(data->amplitudePriorFlag)
    {
    Amin=0.0;
    Amax=1.0;
    }
    */
   prior->range[0][0] = tmin;
   prior->range[0][1] = tmax;
   prior->range[1][0] = fmin;
   prior->range[1][1] = fmax;
   prior->range[2][0] = Qmin;
   prior->range[2][1] = Qmax;
  prior->range[3][0] =  Amin;
  prior->range[3][1] =  Amax;
   prior->range[4][0] = 0.0;
   prior->range[4][1] = LAL_TWOPI;
  if(data->chirpletFlag)
  {
    prior->range[5][0] = betamin;
    prior->range[5][1] = betamax;
  }

   prior->TFV    = (tmax-tmin)*(fmax-fmin)*(Qmax-Qmin);
   prior->logTFV = log(prior->TFV);

}

void initialize_data(struct Data *data, double **s, int N, int tsize, double Tobs, int NI, double fmin, double fmax)
{
   int i,ifo, halfN=N/2;

   data->N     = N;
   data->NI    = NI;
   data->tsize = tsize;
   data->fsize = N/tsize;
   data->Tobs  = Tobs;
   data->fmin  = fmin;
   data->fmax  = fmax;
   data->imin  = (int)floor(fmin*Tobs);
   data->imax  = (int)floor(fmax*Tobs);
   data->dt    = Tobs/(double)(tsize);
   data->df    = 0.5/data->dt;

   // allocate data vectors
   data->h     = dvector(0,N-1); // workspace for individual templates
   data->s     = dmatrix(0,NI-1,0,N-1); // data
   data->r     = dmatrix(0,NI-1,0,N-1); // residual
   data->rh    = dmatrix(0,NI-1,0,N-1);

  // number of signal polarizations (0==h+, 1==hx, ...)
  data->Npol = 2;
  if(data->polarizationFlag) data->Npol = 1;


   // initialize data vectors
   for(i=0; i<N; i++)
   {
      data->h[i] = 0.0;

      for(ifo=0; ifo<NI; ifo++)
      {
         data->s[ifo][i]  = s[ifo][i];
         data->r[ifo][i]  = 0.0;
         data->rh[ifo][i] = 0.0;
      }
   }

  data->ORF = dvector(0,halfN-1);

   data->logLc = 0.0;

  /*
   Initialize logZ to logL of NULL model, i.e. -(d|d)/2
   That way if models are skipped the Bayes factors tell
   us about the improvement over the naive Gaussian noise
   model
   */
  data->logZglitch = data->logLc;
  data->logZnoise  = data->logLc;
  data->logZsignal = data->logLc;
  data->logZfull   = data->logLc;

  data->varZglitch = 0.0;
  data->varZnoise  = 0.0;
  data->varZsignal = 0.0;
  data->varZfull   = 0.0;

    data->cleanFlag  = 0;
    data->glitchFlag = 0;
    data->signalFlag = 0;

}

void initialize_model(struct Model *model, struct Data *data, struct Prior *prior, double **psd, long *seed)
{
  int i,ifo;
  int N = data->N;
  int halfN = data->N/2;
  int NI = data->NI;
  int Npol = data->Npol;
  int NW = data->NW; //number of intrinsic parameters / frame (5 for wavelet)
  
  
  //gaussian noise model
  model->Sn    = dvector(0,NI-1);
  model->SnS   = dmatrix(0,NI-1,0,halfN-1);
  model->Snf   = dmatrix(0,NI-1,0,halfN-1);
  model->invSnf= dmatrix(0,NI-1,0,halfN-1);
  model->SnGeo = dvector(0,halfN-1);

  //store sum of 1/psd in 1/SnGeo
  for(i=0; i<halfN; i++)
  {
    model->SnGeo[i] = 0.0;
    for(ifo=0; ifo<NI; ifo++)
    {
      model->Snf[ifo][i] = psd[ifo][i];
      model->SnS[ifo][i] = psd[ifo][i];
      model->invSnf[ifo][i] = 1./psd[ifo][i];
      model->SnGeo[i] += 1./psd[ifo][i];
    }
    model->SnGeo[i] = 1./model->SnGeo[i];
  }

  //extrinsic parameters
  model->extParams = dvector(0,NE);

  //full instrument response model (projected geocenter + glitches)
  model->response = dmatrix(0,NI-1,0,N-1);

  //incident signal model
  model->h = dmatrix(0,Npol,0,N-1);

  //likelhood is a pointer to a function which returns logL
  model->logL = 0.0;

  //detLogL stores the likelihood in each detector (for quick computation of full logL)
  model->detLogL = dvector(0,NI-1);

  //draw extrinsic parameters from prior
  extrinsic_uniform_proposal(seed,model->extParams);

  //set "delta" parameters to not modify signal model
  model->extParams[4] = 0.0;  //geocenter phase shift
  model->extParams[5] = 1.0;  //amplitude scale


  //allocate memory for signal wavelet structure
  /*
   New for O3: signal structure is a 2-element vector for (+) and (x) polarizations
   */
  model->Npol = Npol;

  model->signal = malloc(Npol * sizeof(struct Wavelet *));
  for(i=0; i<Npol; i++)
  {
    model->signal[i] = malloc(sizeof(struct Wavelet));
    
    //assign wavelet dimension for signal model
    model->signal[i]->dimension = NW; //wavelets
    
    //Setup wavelet structure holding signal model
    if(data->signalFlag)
      initialize_wavelet(model->signal[i],N, prior->smax, 1);
    else
      initialize_wavelet(model->signal[i],N, prior->smax, 0);
  }

  //allocate memory for structure holding projection coefficients
  model->projection = malloc(sizeof(struct Network));

  //set up projection coefficients
  initialize_network(model->projection, N, NI);

  //Allocate and initialize glitch structures
  model->glitch = malloc(NI * sizeof(struct Wavelet *));
  

  for(i=0; i<NI; i++) model->glitch[i] = malloc(sizeof(struct Wavelet));
  for(i=0; i<NI; i++)
  {
    //assign wavelet dimension for glitch model
    model->glitch[i]->dimension = NW; //wavelets

    if(data->glitchFlag)
      initialize_wavelet(model->glitch[i],N, prior->smax, 1);
    else
      initialize_wavelet(model->glitch[i],N, prior->smax, 0);
  }

  //allocate memory for structure holding extrinsic fisher matrix
  model->fisher = malloc(sizeof(struct FisherMatrix));
  initialize_fisher(model->fisher, NE);

  //allocate memory for structure holding intrinsic fisher matrix
  model->intfisher = malloc(sizeof(struct FisherMatrix));
  initialize_fisher(model->intfisher, NW);

  //select wavelet basis function
  if(data->chirpletFlag)
  {
    model->wavelet           = ChirpletFourier;
    model->wavelet_bandwidth = ChirpletBandwidth;
  }
  else
  {
    model->wavelet           = SineGaussianFourier;
    model->wavelet_bandwidth = SineGaussianBandwidth;
  }
  

  //Stochastic background
  if(data->stochasticFlag)
  {
    model->background = malloc(sizeof(struct Background));
    initialize_background(model->background, NI, halfN);
  }
    
}

void reset_model(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model)
{
   int i,j,ic,ifo;
   int NC= chain->NC;
   int NI= data->NI;

   struct Wavelet **signal;
   struct Wavelet **glitch;

   for(ic=0; ic<NC; ic++)
   {
      //reset model
      signal = model[ic]->signal;
      glitch = model[ic]->glitch;
      model[ic]->size=0;
      for(ifo=0; ifo<NI; ifo++) glitch[ifo]->size=0;

     for(i=0; i<model[ic]->Npol; i++)
     {
       signal[i]->size=0;
       signal[i]->smax = prior->smax;
       for(j=1; j<=signal[i]->smax; j++) signal[i]->index[j]=j;
     }
     
     for(i=0; i<NI; i++)
     {
       glitch[i]->smax = prior->smax;
       for(j=1; j<=glitch[i]->smax; j++) glitch[i]->index[j]=j;
      }

      for(ifo=0; ifo<NI; ifo++)
      {
         if(data->glitchFlag)
         {
            glitch[ifo]->size = 1;
            model[ic]->size++;
         }
      }

      if(data->signalFlag)
      {
        for(i=0; i<model[ic]->Npol; i++)
        {
         signal[i]->size = 1;
        }
         model[ic]->size++;
      }

      model[ic]->logL=data->logLc;
   }
}

void free_bayesline(struct BayesLineParams **bayesline, struct Data *data)
{
  int ifo;

  for(ifo=0; ifo<data->NI; ifo++)
  {
    free(bayesline[ifo]->priors->upper);
    free(bayesline[ifo]->priors->lower);
    free(bayesline[ifo]->priors->mean);
    free(bayesline[ifo]->priors->sigma);
    free(bayesline[ifo]->priors);
    BayesLineFree(bayesline[ifo]);
  }
}


void initialize_bayesline(struct BayesLineParams **bayesline, struct Data *data, double **psd, int nstep)
{
  int i,ifo;

  int N    = (int)(data->Tobs*(data->fmax-data->fmin))+1;
  int imin = (int)(data->Tobs*data->fmin);

  for(ifo=0; ifo<data->NI; ifo++)
  {
    bayesline[ifo] = malloc(sizeof(struct BayesLineParams));
    bayesline[ifo]->priors = malloc(sizeof(BayesLinePriors));
    //bayesline[ifo]->priors->invsigma = malloc((int)(Tobs*(fmax-fmin))*sizeof(double));
    bayesline[ifo]->priors->upper = malloc(N*sizeof(double));
    bayesline[ifo]->priors->lower = malloc(N*sizeof(double));
    bayesline[ifo]->priors->mean  = malloc(N*sizeof(double));
    bayesline[ifo]->priors->sigma = malloc(N*sizeof(double));

    //Set BayesLine priors based on the data channel being used
    set_bayesline_priors(data->channels[ifo], bayesline[ifo], data->Tobs);

    // set default flags
    bayesline[ifo]->constantLogLFlag = data->constantLogLFlag;

    //Use initial estimate of PSD to set priors
    for(i=0; i<N; i++)
    {
      //bayesline[ifo]->priors->invsigma[i] = 1./(psd[ifo][i+(int)(Tobs*fmin)]*1.0);
      bayesline[ifo]->priors->sigma[i] = psd[ifo][i+imin];
      bayesline[ifo]->priors->mean[i]  = psd[ifo][i+imin];
      bayesline[ifo]->priors->lower[i] = psd[ifo][i+imin]/100.;
      bayesline[ifo]->priors->upper[i] = psd[ifo][i+imin]*2.;

    }

    //Allocates arrays and sets constants for for BayesLine
    BayesLineSetup(bayesline[ifo], data->s[ifo], data->fmin, data->fmax, data->dt, data->Tobs, nstep);
  }

//  FILE *fptr=fopen("psdprior.dat","w");
//  for(i=0; i<(int)(data->Tobs*(data->fmax-data->fmin)); i++)
//  {
//    fprintf(fptr,"%lg ",data->fmin + i*1./data->Tobs);
//    for(ifo=0; ifo<data->NI; ifo++)
//    {
//      fprintf(fptr,"%lg ",psd[ifo][i+(int)(data->Tobs*data->fmin)]);
//      fprintf(fptr,"%lg ",bayesline[ifo]->priors->lower[i]);
//      fprintf(fptr,"%lg ",bayesline[ifo]->priors->upper[i]);
//    }
//    fprintf(fptr,"\n");
//  }
//  fclose(fptr);
}


void reset_likelihood(struct Data *data)
{
    if(data->constantLogLFlag)
    {
        // logL=const to test detailed balance
        data->intrinsic_likelihood = EvaluateConstantLogLikelihood;
        data->extrinsic_likelihood = EvaluateExtrinsicConstantLogLikelihood;
    }
    else
    {
        data->intrinsic_likelihood = EvaluateMarkovianLogLikelihood;
        data->extrinsic_likelihood = EvaluateExtrinsicMarkovianLogLikelihood;
    }
}

void initialize_network(struct Network *projection, int N, int NI)
{
    projection->expPhase = malloc(NI*sizeof(double *));
    projection->deltaT   = malloc(NI*sizeof(double));
    projection->Fplus    = malloc(NI*sizeof(double));
    projection->Fcross   = malloc(NI*sizeof(double));
    projection->dtimes   = malloc(NI*sizeof(double));

    int i;
    for(i=0; i<NI; i++) projection->expPhase[i] = malloc(N*sizeof(double));
}

void free_network(struct Network *projection, int NI)
{
    int i;
    for(i=0; i<NI; i++) free(projection->expPhase[i]);

    free(projection->expPhase);
    free(projection->deltaT);
    free(projection->Fplus);
    free(projection->Fcross);
    free(projection->dtimes);
}

void copy_int_model(struct Model *origin, struct Model *copy, int N, int NI, int det)
{
   int i,ip,ifo;

   copy->logL=origin->logL;

   copy->size=origin->size;
  
   copy->Npol=origin->Npol;

   if(det==-1)
   {
      for(ip=0;  ip<origin->Npol;  ip++) copy_wavelet(origin->signal[ip], copy->signal[ip], N);
      for(ip=0;  ip<=origin->Npol; ip++) for(i=0; i<N; i++) copy->h[ip][i] = origin->h[ip][i];
      for(ifo=0; ifo<NI;          ifo++) for(i=0; i<N; i++) copy->response[ifo][i] = origin->response[ifo][i];
   }
   else
   {
      copy_wavelet(origin->glitch[det],copy->glitch[det], N);
   }

   for(ifo=0; ifo<NI; ifo++)
   {
      copy->detLogL[ifo] = origin->detLogL[ifo];

      copy->Sn[ifo] = origin->Sn[ifo]; //noise PSD scale
   }
}

void copy_psd_model(struct Model *origin, struct Model *copy, int N, int NI)
{
  int n,ifo;
  int halfN = N/2;
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<halfN; n++)
    {
      copy->SnS[ifo][n]    = origin->SnS[ifo][n];
      copy->Snf[ifo][n]    = origin->Snf[ifo][n];
      copy->invSnf[ifo][n] = origin->invSnf[ifo][n];
    }
  }
  for(n=0; n<halfN; n++)
  {
    copy->SnGeo[n] = origin->SnGeo[n];
  }
}

void copy_ext_model(struct Model *origin, struct Model *copy, int N, int NI)
{
    int i,ifo;
    for(i=0; i<NE; i++) copy->extParams[i] = origin->extParams[i];

    for(ifo=0; ifo<NI; ifo++)
    {
        copy->projection->Fplus[ifo]  = origin->projection->Fplus[ifo];
        copy->projection->Fcross[ifo] = origin->projection->Fcross[ifo];
        copy->projection->deltaT[ifo] = origin->projection->deltaT[ifo];
        copy->projection->dtimes[ifo] = origin->projection->dtimes[ifo];
        for(i=0; i<N; i++) copy->projection->expPhase[ifo][i] = origin->projection->expPhase[ifo][i];
    }
}

void free_model(struct Model *model, struct Data *data, struct Prior *prior)
{
  int i;
  int N = data->N;
  int NI = data->NI;
  int halfN = data->N/2;
  int Npol = model->Npol;
  
  free_dvector(model->Sn,0,NI-1);
  free_dvector(model->SnGeo,0,halfN-1);
  free_dmatrix(model->SnS,0,NI-1,0,halfN-1);
  free_dmatrix(model->Snf,0,NI-1,0,halfN-1);
  free_dmatrix(model->invSnf,0,NI-1,0,halfN-1);

  free_dvector(model->detLogL,0,NI-1);
  free_dvector(model->extParams,0,NE);
  free_dmatrix(model->response,0,NI-1,0,N-1);
  free_dmatrix(model->h,0,Npol,0,N-1);

  for(i=0; i<Npol; i++)
  {
    free_wavelet(model->signal[i],N, prior->smax);
    free(model->signal[i]);
  }
  free(model->signal);
  
  free_network(model->projection, NI);
  free(model->projection);

  free_fisher(model->fisher);
  free(model->fisher);

  free_fisher(model->intfisher);
  free(model->intfisher);

  for(i=0; i<NI; i++)
  {
    free_wavelet(model->glitch[i],N,prior->smax);
    free(model->glitch[i]);
  }
  free(model->glitch);

  if(data->stochasticFlag)
  {
    free_background(model->background, NI, halfN);
    free(model->background);
  }
}

void initialize_wavelet(struct Wavelet *wave, int N, int smax, int size)
{
   int j;

   // initial number of wavelets
   wave->size = size;

   // maximum number of basis functions
   /*
    redundent to make Wavelet more self-contained
    should consider doing the same with N, and for
    other structures as well...
    //TODO: give all structures a copy of N??
    */
   wave->smax = smax;

   //store wavelet parameters
   wave->intParams = dmatrix(1,smax,0,wave->dimension-1);

   // this keeps track of who is who
   wave->index = ivector(1,smax);

   // initialize reference order
   for(j=1; j<= smax; j++) wave->index[j] = j;

   // Fourier domain linear combination of wavelets
   wave->templates = dvector(0,N-1);
   for(j=0; j<N; j++) wave->templates[j] = 0.0;

   // clustering prior
   wave->Ncluster = 0;
   wave->cosy = 0.0;
   wave->logp = 0.0;

}

void copy_wavelet(struct Wavelet *origin, struct Wavelet *copy, int N)
{
   int i,j, iend=origin->smax+1;

  for(i=1; i<iend; i++)
   {
      copy->index[i] = origin->index[i];
      for(j=0; j<origin->dimension; j++) copy->intParams[i][j] = origin->intParams[i][j];
   }
   for(j=0; j<N; j++) copy->templates[j] = origin->templates[j];

   copy->size      = origin->size;
   copy->dimension = origin->dimension;

   // clustering prior
   copy->Ncluster = origin->Ncluster;
   copy->cosy     = origin->cosy;
   copy->logp     = origin->logp;

}

void free_wavelet(struct Wavelet *wave, int N, int smax)
{
   free_dmatrix(wave->intParams,1,smax,0,wave->dimension-1);
   free_ivector(wave->index,1,smax);
   free_dvector(wave->templates,0,N-1);
}

void initialize_background(struct Background *background, int NI, int N)
{
  background->Cij      = d3tensor(0,NI-1,0,NI-1,0,N-1);
  background->detCij   = dvector(0,N-1);
  background->spectrum = dvector(0,N-1);
  background->logamp=-100.0;
  background->index=1.0;
  background->fref=100.0;
  background->invfref=1./background->fref;
}

void copy_background(struct Background *origin, struct Background *copy, int NI, int N)
{
  int n,i,j;

  copy->logamp  = origin->logamp;
  copy->index   = origin->index;
  copy->fref    = origin->fref;
  copy->invfref = origin->invfref;

  for(n=0; n<N; n++)
  {
    copy->spectrum[n] = origin->spectrum[n];
    copy->detCij[n]   = origin->detCij[n];
  }
  for(i=0; i<NI; i++)
  {
    for(j=0; j<NI; j++)
    {
      for(n=0; n<N; n++)
      {
        copy->Cij[i][j][n] = origin->Cij[i][j][n];
      }
    }
  }
}

void free_background(struct Background *background, int NI, int N)
{
  free_d3tensor(background->Cij,0,NI-1,0,NI-1,0,N-1);
  free_dvector(background->detCij,0,N-1);
  free_dvector(background->spectrum,0,N-1);
}

void initialize_TF_proposal(struct Data *data, struct Prior *prior, struct TimeFrequencyMap *tf)
{
  int ifo;
  //int i,ifo;
  //int N =data->N;
  int NI=data->NI;

  tf->N = data->N;

  //4s grid for Q
  tf->nQ = (int)(prior->range[2][1] - prior->range[2][0])/2;

  //4Hz grid for frequency (normalized to 1024Hz sampling rate)
  tf->nf = (int)((prior->range[1][1] - prior->range[1][0])/(4.*((data->fmax*2.)/1024.)));

//  double tmin = prior->range[0][0];
//  double tmax = prior->range[0][1];
//
//  tf->nt = 0;
//  double t;
//  for(i = 0; i < N; i++)
//  {
//    t = ((double)(i)+0.5)*(8*data->Tobs/data->N);
//    if(t > tmin && t < tmax) tf->nt++;
//  }
  //.005s grid for t (normalized to 2s seglen)
  //FIXME: Revisit grid spacing--is this optimal for the large TFVs?
  tf->nt = (int)((prior->range[0][1] - prior->range[0][0])/(0.005*(data->Tobs/2.) )); //1024;

  tf->pdf = malloc((NI+1)*sizeof(double ***));
  tf->snr = malloc((NI+1)*sizeof(double ***));
  tf->max = malloc((NI+1)*sizeof(double *));

  for(ifo=0; ifo<NI+1; ifo++)
  {
    tf->pdf[ifo] = d3tensor(0,tf->nQ-1,0,tf->nf-1,0,tf->nt-1);
    tf->snr[ifo] = d3tensor(0,tf->nQ-1,0,tf->nf-1,0,tf->nt-1);
    tf->max[ifo] = malloc(tf->nQ*sizeof(double));

  }
}

void free_TF_proposal(struct Data *data, struct TimeFrequencyMap *tf)
{
  int ifo;

  for(ifo=0; ifo<data->NI+1; ifo++)
  {
    free_d3tensor(tf->pdf[ifo],0,tf->nQ-1,0,tf->nf-1,0,tf->nt-1);
    free_d3tensor(tf->snr[ifo],0,tf->nQ-1,0,tf->nf-1,0,tf->nt-1);
    free(tf->max[ifo]);

  }

  free(tf->pdf);
  free(tf->snr);
  free(tf->max);
  
}

int checkfile(char *name)
{
   FILE *fp=fopen(name,"r");
   if(fp)
   {
      fclose(fp);
      return 1;
   }
   else return 0;
}


int *ivector(long nl, long nh)
/* allocate an int vector with subscript range v[nl..nh] */
{
   int *v;

   v=(int *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(int)));
   if (!v) fprintf(stderr,"allocation failure in ivector()");
   return v-nl+NR_END;
}

void free_ivector(int *v, long nl, UNUSED long nh)
/* free an int vector allocated with ivector() */
{
   free((FREE_ARG) (v+nl-NR_END));
}

double *dvector(long nl, long nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
   double *v=0;

   v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
   if (!v) fprintf(stderr,"allocation failure in dvector()");
   return v-nl+NR_END;
}

void free_dvector(double *v, long nl, UNUSED long nh)
/* free a double vector allocated with dvector() */
{
   free((FREE_ARG) (v+nl-NR_END));
}

unsigned long *lvector(long nl, long nh)
/* allocate an unsigned long vector with subscript range v[nl..nh] */
{
   unsigned long *v;

   v=(unsigned long *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(long)));
   if (!v) fprintf(stderr,"allocation failure in lvector()");
   return v-nl+NR_END;
}

void free_lvector(unsigned long *v, long nl, UNUSED long nh)
/* free an unsigned long vector allocated with lvector() */
{
   free((FREE_ARG) (v+nl-NR_END));
}

int **imatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a int matrix with subscript range m[nrl..nrh][ncl..nch] */
{
   long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
   int **m;

   /* allocate pointers to rows */
   m=(int **) malloc((size_t)((nrow+NR_END)*sizeof(int*)));
   if (!m) fprintf(stderr, "allocation failure 1 in matrix()");
   m += NR_END;
   m -= nrl;


   /* allocate rows and set pointers to them */
   m[nrl]=(int *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int)));
   if (!m) fprintf(stderr, "allocation failure 2 in matrix()");
   m[nrl] += NR_END;
   m[nrl] -= ncl;

   for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

   /* return pointer to array of pointers to rows */
   return m;
}

void free_imatrix(int **m, long nrl, UNUSED long nrh, long ncl, UNUSED long nch)
/* free an int matrix allocated by imatrix() */
{
   free((FREE_ARG) (m[nrl]+ncl-NR_END));
   free((FREE_ARG) (m+nrl-NR_END));
}

double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
   long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
   double **m;

   /* allocate pointers to rows */
   m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
   if (!m) fprintf(stderr, "allocation failure 1 in matrix()");
   m += NR_END;
   m -= nrl;

   /* allocate rows and set pointers to them */
   m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
   if (!m[nrl]) fprintf(stderr,"allocation failure 2 in matrix()");
   m[nrl] += NR_END;
   m[nrl] -= ncl;

   for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

   /* return pointer to array of pointers to rows */
   return m;
}

void free_dmatrix(double **m, long nrl, UNUSED long nrh, long ncl, UNUSED long nch)
/* free a double matrix allocated by dmatrix() */
{
   free((FREE_ARG) (m[nrl]+ncl-NR_END));
   free((FREE_ARG) (m+nrl-NR_END));
}

double ***d3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a double 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
   long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
   double ***t;

   /* allocate pointers to pointers to rows */
   t=(double ***) malloc((size_t)((nrow+NR_END)*sizeof(double**)));
   if (!t) fprintf(stderr,"allocation failure 1 in d3tensor()");
   t += NR_END;
   t -= nrl;

   /* allocate pointers to rows and set pointers to them */
   t[nrl]=(double **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double*)));
   if (!t[nrl]) fprintf(stderr,"allocation failure 2 in d3tensor()");
   t[nrl] += NR_END;
   t[nrl] -= ncl;

   /* allocate rows and set pointers to them */
   t[nrl][ncl]=(double *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(double)));
   if (!t[nrl][ncl]) fprintf(stderr,"allocation failure 3 in f3tensor()");
   t[nrl][ncl] += NR_END;
   t[nrl][ncl] -= ndl;

   for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
   for(i=nrl+1;i<=nrh;i++) {
      t[i]=t[i-1]+ncol;
      t[i][ncl]=t[i-1][ncl]+ncol*ndep;
      for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
   }

   /* return pointer to array of pointers to rows */
   return t;
}

void free_d3tensor(double ***t, long nrl, UNUSED long nrh, long ncl, UNUSED long nch, long ndl, UNUSED long ndh)
/* free a float d3tensor allocated by d3tensor() */
{
   free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
   free((FREE_ARG) (t[nrl]+ncl-NR_END));
   free((FREE_ARG) (t+nrl-NR_END));
}

int ***i3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a double 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
   long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
   int ***t;
   
   /* allocate pointers to pointers to rows */
   t=(int ***) malloc((size_t)((nrow+NR_END)*sizeof(int**)));
   if (!t) fprintf(stderr,"allocation failure 1 in d3tensor()");
   t += NR_END;
   t -= nrl;
   
   /* allocate pointers to rows and set pointers to them */
   t[nrl]=(int **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int*)));
   if (!t[nrl]) fprintf(stderr,"allocation failure 2 in d3tensor()");
   t[nrl] += NR_END;
   t[nrl] -= ncl;
   
   /* allocate rows and set pointers to them */
   t[nrl][ncl]=(int *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(int)));
   if (!t[nrl][ncl]) fprintf(stderr,"allocation failure 3 in f3tensor()");
   t[nrl][ncl] += NR_END;
   t[nrl][ncl] -= ndl;
   
   for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
   for(i=nrl+1;i<=nrh;i++) {
      t[i]=t[i-1]+ncol;
      t[i][ncl]=t[i-1][ncl]+ncol*ndep;
      for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
   }
   
   /* return pointer to array of pointers to rows */
   return t;
}

void free_i3tensor(int ***t, long nrl, UNUSED long nrh, long ncl, UNUSED long nch, long ndl, UNUSED long ndh)
/* free an int d3tensor allocated by i3tensor() */
{
   free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
   free((FREE_ARG) (t[nrl]+ncl-NR_END));
   free((FREE_ARG) (t+nrl-NR_END));
}
