/*   Compile:
 gcc -O2 deglitcher.c -lm -o deglitcher -lgsl
 */

/***************************  REQUIRED LIBRARIES  ***************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_cdf.h>

#include <lal/LALCache.h>
#include <lal/LALFrStream.h>
#include <lal/TimeSeries.h>
#include <lal/XLALError.h>
#include <lal/Date.h>


/*************  PROTOTYPE DECLARATIONS FOR INTERNAL FUNCTIONS  **************/

double swap, tempr;
#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}

#define REQARG 12

struct Data
{
  int medianFlag;

  //Set data products from command line
  char ifo[4];        //interferometer (e.g V1);

  char fr_cache[1024];//cache file (e.g. V1.cache)
  char fr_chanl[1024];//channel type (e.g. V1:Hrec_hoft_16384Hz)
  char fr_type[1024]; //frame type (e.g. V1_llhoft)

  double fr_seglen;   //frame segment length
  double fr_start;    //frame start time
  double fr_srate;    //frame sampling rate

  char bw_model[1024]; //bayeswave glitch model

  double bw_seglen;   //bayeswave segment length
  double bw_start;    //bayeswave start time
  double bw_srate;    //bayeswave sampling rate
  double bw_trigtime; //bayeswave trigger time

  //Derived data products
  int bw_N;           //bayeswave number of samples
  double bw_dt;       //bayeswave sample length

};

void print_usage();
void parse_command_line(int argc, char **argv, struct Data *data);

static REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length);

static void output_frame(REAL8TimeSeries *timeData, REAL8TimeSeries *timeRes, REAL8TimeSeries *timeGlitch, CHAR *frameType, CHAR *ifo);

void dfour1(double data[], unsigned long nn, int isign);
void drealft(double data[], unsigned long n, int isign);

void tukey_scale(double *s1, double *s2, double alpha, int N);
void tukey(double *data, double alpha, int N);


/* ============================  MAIN PROGRAM  ============================ */


int main(int argc, char *argv[])
{
  fprintf(stdout,"\n");
  fprintf(stdout,"#############################################################\n");
  fprintf(stdout,"                BayesWave Glitch Subtraction                 \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                             **                              \n");
  fprintf(stdout,"                            * *                              \n");
  fprintf(stdout,"****************************  *   ***************************\n");
  fprintf(stdout,"                              * **                           \n");
  fprintf(stdout,"                              **                             \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                                                             \n");
  fprintf(stdout,"#############################################################\n");
  fprintf(stdout,"\n");

  /*   Variable declaration   */
  int i, j, k, N;

  double x;
  double dT;

  double alpha;

  int Nup;
  double norm;
  double s1, s2;
  double t_rise;

  struct Data *data = malloc(sizeof(struct Data));

  FILE *infile;

  // ------------------------------------------------- //
  parse_command_line(argc, argv, data);

  REAL8TimeSeries* timeRes=NULL;
  REAL8TimeSeries* timeData=NULL;
  REAL8TimeSeries* timeGlitch=NULL;
  LIGOTimeGPS epoch;

  // Set time & retrieve data by reading frame cache
  XLALGPSSetREAL8(&epoch, data->fr_start);
  fprintf(stdout,"Reading original data:\n");
  fprintf(stdout,"     Type:    %s\n",data->fr_type);
  fprintf(stdout,"     Channel: %s\n",data->fr_chanl);
  fprintf(stdout,"\n");
  timeRes    = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);
  timeData   = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);
  timeGlitch = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);

  char * version="T1700406_v3";

  char outframeType[128];
  char outframeChannel[128];
  char outframeGlitchChannel[128];

  sprintf(outframeType,   "%s_%s", data->fr_type,  version);
  sprintf(outframeChannel,"%s_%s", data->fr_chanl, version);
  sprintf(outframeGlitchChannel,"%s_glitch", data->fr_chanl);

  /* set the channel name */
  strncpy(timeData->name,   data->fr_chanl,        LALNameLength);
  strncpy(timeRes->name,    outframeChannel,       LALNameLength);
  strncpy(timeGlitch->name, outframeGlitchChannel, LALNameLength);

  
  fprintf(stdout,"Creating glitch-subtracted frame:\n");
  fprintf(stdout,"     Type:    %s\n",outframeType);
  fprintf(stdout,"     Channel: %s\n",outframeChannel);
  fprintf(stdout,"\n");

  // shortcuts to original frame size/cadence
  dT = timeData->deltaT;
  N  = timeData->data->length;

  // parameters for BW glitch data
  data->bw_dt = 1.0/data->bw_srate;
  data->bw_N  = data->bw_seglen/data->bw_dt;

  // Length of the up-sampled glitch data
  Nup = (int)(data->bw_seglen*data->fr_srate);
  if(Nup<data->bw_N)
  {
    fprintf(stdout,"\nbayeswave_cleanframe: model has higher sampling rate than frame\n\n");
    exit(0);
  }

  double *clean_data             = malloc(sizeof(double)*N);
  double *glitch_time            = malloc(sizeof(double)*data->bw_N);
  double *glitch_model           = malloc(sizeof(double)*data->bw_N);
  double *glitch_model_resampled = malloc(sizeof(double)*Nup);
  double *glitch_model_padded    = malloc(sizeof(double)*N);
  double **glitch_array          = malloc(sizeof(double *)*data->bw_N);
  for(i=0; i<data->bw_N; i++) glitch_array[i] = malloc(sizeof(double)*100);

  // read in BW glitch reconstruction
  if(data->medianFlag)
  {
    //split glitch model file name
    char start[1024];
    char extension[1024];
    j = 0;
    while(data->bw_model[j]!='1')
    {
      start[j]=data->bw_model[j];
      j++;
    }

    //burn off next two digits
    j+=3;
    i=0;
    while(j<1024)
    {
      extension[i]=data->bw_model[j];
      j++;
      i++;
    }

    fprintf(stdout,"Using median glitch reconstruction:\n");
    fprintf(stdout,"     Files: %s[%i-%i]%s\n",start,100,199,extension);
    fprintf(stdout,"\n");

    //read all waveform reconstruction into array
    char filename[1024];
    for(j=0; j<100; j++)
    {
      sprintf(filename,"%s%i%s",start,100+j,extension);
      infile=fopen(filename,"r");
      for(i=0; i< data->bw_N; i++)
      {
        fscanf(infile,"%lf%lf", &x, &glitch_array[i][j]);
        if(j==0)glitch_time[i] = data->bw_start + (double)(i)*data->bw_dt;
      }
      fclose(infile);
    }

    //get median of each time bin
    for(i=0; i<data->bw_N; i++)
    {
      gsl_sort(glitch_array[i], 1, 100);
      glitch_model[i] = gsl_stats_median_from_sorted_data(glitch_array[i], 1, 100);
    }
  }
  else
  {
    infile = fopen(data->bw_model,"r");

    for(i=0; i< data->bw_N; i++)
    {
      fscanf(infile,"%lf%lf", &x, &glitch_model[i]);
      glitch_time[i] = data->bw_start + (double)(i)*data->bw_dt;
    }
    fclose(infile);
  }


  // Set up Tukey window parameter. Flat for (1-alpha) of data
  t_rise = 0.4;  // the standard LIGO choice
  alpha  = (2.0*t_rise/data->bw_seglen);

  // Tukey filer the glitch reconstruction to avoid edge effects
  tukey(glitch_model, alpha, data->bw_N);

  // FFT
  drealft(glitch_model-1, data->bw_N, 1);

  // FFT scaling
  norm = 2.0/(double)data->bw_N;

  // zero pad the high frequencies
  for(i=0; i< data->bw_N; i++)
  {
    glitch_model[i] *= norm;
    glitch_model_resampled[i] = glitch_model[i];
  }
  for(i=data->bw_N; i< Nup; i++)
  {
    glitch_model_resampled[i] = 0.0;
  }

  // Inverse FFT
  drealft(glitch_model_resampled-1, Nup, -1);


  /***********************************************************
   glitch_model_resampled now contains the glitch model,
   upsampled to the original cadence
   ************************************************************/

  // figure out where to place the model in the frame
  j = (int)((data->bw_start-data->fr_start)/dT);
  k = j+Nup;

  //zero pad array
  for(i=0; i< N; i++) glitch_model_padded[i] = 0.0;

  //place glitch model in right part of full array
  for(i=j; i< k; i++)
  {
    glitch_model_padded[i] = glitch_model_resampled[i-j];
  }

  /***********************************************************
   glitch_model_padded now contains the glitch model,
   over the same interval & cadence as the original frame
  ************************************************************/

  //renormalize glitch model amplitude (undo windowing)
  tukey_scale(&s1, &s2, alpha, N);

  norm = sqrt(1.0/(data->bw_dt*data->bw_seglen))*s2;

  // Initialise and populate clean data
  for(i=0; i< N; i++)
  {
    //store clean data in it's own array for spectrogram making
    clean_data[i] = timeData->data->data[i] - norm*glitch_model_padded[i];

    //fill LAL data structure with cleaned data for frame-making
    timeRes->data->data[i] = clean_data[i];
    
    //fill another structure with glitch model
    timeGlitch->data->data[i] = norm*glitch_model_padded[i];
  }


  // Output cleaned data!
  output_frame(timeData, timeRes, timeGlitch, outframeType, data->ifo);
  XLALDestroyREAL8TimeSeries(timeData);


//  printf("whitening a 32 segment surrounding the glitch and producing a spectrogram\n");
//  FILE *outfile;
//  double Tprint = 32.0;
//// Used to check cleaning
//  N32 = (int)(Tprint/dT);
  //  double *G32 = malloc(N32*sizeof(double));
  //  double *S32 = malloc(N32*sizeof(double));
  //  double *L = malloc(N*sizeof(double));

  //  const gsl_rng_type * P;
//  gsl_rng * r;
//  gsl_rng_env_setup();
//  P = gsl_rng_default;
//  r = gsl_rng_alloc (P);
//
//  double f;
//  j -= N32/2;
//
//  if(j < 0)
//  {
//    j = 0;
//  }
//
//  if(j > N-N32)
//  {
//    j = N-N32;
//  }
//
//  for(i=0; i< N32; i++)
//  {
//    G32[i] = Lclean[i+j];
//  }
//
//  alpha = (2.0*t_rise/32.0);
//
//  tukey(G32, alpha, N32);
//  drealft(G32-1, N32, 1);
//
//  double *SL32, *SLn32;
//
//  SL32 = dvector(0,N32/2-1);
//  SLn32 = dvector(0,N32/2-1);
//
//  // Power spectra
//  for(i=0; i< N32/2; i++) SL32[i] = 2.0*(G32[2*i]*G32[2*i]+G32[2*i+1]*G32[2*i+1]);
//
//  // Form spectral model for whitening data (lines plus a smooth component)
//  spectrum(SL32, SLn32, 1.0/Tprint, r, N32);
//
//  printf("spectrum computed\n");
//
//  outfile=fopen("spec32.dat","w");
//  for(i=1;i< N32/2;i++)
//  {
//    f = (double)(i)/32.0;
//    fprintf(outfile,"%e %e\n", f,  SLn32[i]);
//  }
//  fclose(outfile);
//
//  printf("computing Q transform\n");
//  QTransfrom(20.0, G32, SLn32, 8.0, 1024.0, Tprint, N32, 4);
//
//  printf("plotting Q transform\n");
//  system ("gnuplot specto.gnu");
//  system ("open Qspec.png");
//
  fprintf(stdout,"\n");
  return 0;
}

void tukey_scale(double *s1, double *s2, double alpha, int N)
{
  int i, imin, imax;
  double x1, x2;
  double filter;

  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));

  x1 = 0.0;
  x2 = 0.0;
  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i < imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-2.0/alpha+1.0 )));
    x1 += filter;
    x2 += filter*filter;
  }
  x1 /= (double)(N);
  x2 /= (double)(N);

  *s1 = x1;
  *s2 = sqrt(x2);

}

void tukey(double *data, double alpha, int N)
{
  int i, imin, imax;
  double filter;

  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));

  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i < imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-2.0/alpha+1.0 )));
    data[i] *= filter;
  }

}


/* ********************************************************************************** */
/*																					  */
/*                                 Fourier Routines                                   */
/*																					  */
/* ********************************************************************************** */

void dfour1(double data[], unsigned long nn, int isign)
{
  unsigned long n,mmax,m,j,istep,i;
  double wtemp,wr,wpr,wpi,wi,theta;
  double tempr,tempi, swap;

  n=nn << 1;
  j=1;
  for (i=1;i<n;i+=2) {
    if (j > i) {
      SWAP(data[j],data[i]);
      SWAP(data[j+1],data[i+1]);
    }
    m=n >> 1;
    while (m >= 2 && j > m) {
      j -= m;
      m >>= 1;
    }
    j += m;
  }
  mmax=2;
  while (n > mmax) {
    istep=mmax << 1;
    theta=isign*(6.28318530717959/mmax);
    wtemp=sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi=sin(theta);
    wr=1.0;
    wi=0.0;
    for (m=1;m<mmax;m+=2) {
      for (i=m;i<=n;i+=istep) {
        j=i+mmax;
        tempr=wr*data[j]-wi*data[j+1];
        tempi=wr*data[j+1]+wi*data[j];
        data[j]=data[i]-tempr;
        data[j+1]=data[i+1]-tempi;
        data[i] += tempr;
        data[i+1] += tempi;
      }
      wr=(wtemp=wr)*wpr-wi*wpi+wr;
      wi=wi*wpr+wtemp*wpi+wi;
    }
    mmax=istep;
  }
}

void drealft(double data[], unsigned long n, int isign)
{
  void dfour1(double data[], unsigned long nn, int isign);
  unsigned long i,i1,i2,i3,i4,np3;
  double c1=0.5,c2,h1r,h1i,h2r,h2i;
  double wr,wi,wpr,wpi,wtemp,theta;

  theta=3.141592653589793/(double) (n>>1);
  if (isign == 1) {
    c2 = -0.5;
    dfour1(data,n>>1,1);
  } else {
    c2=0.5;
    theta = -theta;
  }
  wtemp=sin(0.5*theta);
  wpr = -2.0*wtemp*wtemp;
  wpi=sin(theta);
  wr=1.0+wpr;
  wi=wpi;
  np3=n+3;
  for (i=2;i<=(n>>2);i++) {
    i4=1+(i3=np3-(i2=1+(i1=i+i-1)));
    h1r=c1*(data[i1]+data[i3]);
    h1i=c1*(data[i2]-data[i4]);
    h2r = -c2*(data[i2]+data[i4]);
    h2i=c2*(data[i1]-data[i3]);
    data[i1]=h1r+wr*h2r-wi*h2i;
    data[i2]=h1i+wr*h2i+wi*h2r;
    data[i3]=h1r-wr*h2r+wi*h2i;
    data[i4] = -h1i+wr*h2i+wi*h2r;
    wr=(wtemp=wr)*wpr-wi*wpi+wr;
    wi=wi*wpr+wtemp*wpi+wi;
  }
  if (isign == 1) {
    data[1] = (h1r=data[1])+data[2];
    data[2] = h1r-data[2];
  } else {
    data[1]=c1*((h1r=data[1])+data[2]);
    data[2]=c1*(h1r-data[2]);
    dfour1(data,n>>1,-1);
  }
}

// for n odd, median given by k = (n+1)/2. for n even, median is average of k=n/2, k=n/2+1 elements

/* ********************************************************************************** */
/*																					  */
/*                                 Frame I/O                                          */
/*																					  */
/* ********************************************************************************** */

static REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length)
{
  LALStatus status;
  memset(&status,0,sizeof(status));
  LALCache *cache = NULL;
  LALFrStream *stream = NULL;
  REAL8TimeSeries *out = NULL;

  cache  = XLALCacheImport( cachefile );
  int err;
  err = *XLALGetErrnoPtr();
  if(cache==NULL) {fprintf(stderr,"ERROR: Unable to import cache file \"%s\",\n       XLALError: \"%s\".\n",cachefile, XLALErrorString(err)); exit(-1);}
  stream = XLALFrStreamCacheOpen( cache );
  if(stream==NULL) {fprintf(stderr,"ERROR: Unable to open stream from frame cache file\n"); exit(-1);}
  out = XLALFrStreamInputREAL8TimeSeries( stream, channel, &start, length , 0 );
  if(out==NULL) fprintf(stderr,"ERROR: unable to read channel %s from %s at time %i\nCheck the specified data duration is not too long\n",channel,cachefile,start.gpsSeconds);
  XLALDestroyCache(cache);
  LALFrClose(&status,&stream);
  return out;
}


static void output_frame(REAL8TimeSeries *timeData,
                         REAL8TimeSeries *timeRes,
                         REAL8TimeSeries *timeGlitch,
                         CHAR *frameType,
                         CHAR *ifo)
{
  CHAR fname[2048];
  INT4 duration;
  INT8 detectorFlags;
  LALFrameH *frame;

  int gpsStart = timeData->epoch.gpsSeconds;
  int gpsEnd = gpsStart + (int)timeData->data->length*timeData->deltaT;


  /* set detector flags */
  if ( strncmp( ifo, "H2", 2 ) == 0 )
    detectorFlags = LAL_LHO_2K_DETECTOR_BIT;
  else if ( strncmp( ifo, "H1", 2 ) == 0 )
    detectorFlags = LAL_LHO_4K_DETECTOR_BIT;
  else if ( strncmp( ifo, "L1", 2 ) == 0 )
    detectorFlags = LAL_LLO_4K_DETECTOR_BIT;
  else if ( strncmp( ifo, "G1", 2 ) == 0 )
    detectorFlags = LAL_GEO_600_DETECTOR_BIT;
  else if ( strncmp( ifo, "V1", 2 ) == 0 )
    detectorFlags = LAL_VIRGO_DETECTOR_BIT;
  else if ( strncmp( ifo, "T1", 2 ) == 0 )
    detectorFlags = LAL_TAMA_300_DETECTOR_BIT;
  else
  {
    fprintf( stderr, "ERROR: Unrecognised IFO: '%s'\n", ifo );
    exit( 1 );
  }

  /* get frame filename */
  duration = gpsEnd - gpsStart;
  snprintf( fname, FILENAME_MAX, "%c-%s-%d-%d.gwf", ifo[0], frameType, gpsStart, duration );

  /* define frame */
  frame = XLALFrameNew( &timeData->epoch, duration, "LIGO", 0, 1, detectorFlags );

  /* add channel to frame */
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeRes );
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeGlitch );
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeData );

  fprintf( stdout, "Writing data to frame: '%s'\n", fname );

  /* write frame */
  if (XLALFrameWrite( frame, fname) != 0)
  {
    fprintf( stderr, "ERROR: Cannot save frame file: '%s'\n", fname );
    exit( 1 );
  }

  /* clear frame */
  XLALFrameFree( frame );

  return;
}

void parse_command_line(int argc, char **argv, struct Data *data)
{
  int i;

  if(argc==1)
  {
    print_usage();
    exit(0);
  }

  FILE *outfile = fopen("bayeswave_cleanframe.run","w");
  for(i=0; i<argc; i++) fprintf(outfile,"%s ",argv[i]);
  fprintf(outfile,"\n");
  fclose(outfile);

  data->medianFlag = 0;

  static struct option long_options[] =
  {
    {"ifo",           required_argument, 0, 0},
    {"glitch-model",  required_argument, 0, 0},
    {"cachefile",     required_argument, 0, 0},
    {"channel",       required_argument, 0, 0},
    {"frame-type",    required_argument, 0, 0},
    {"frame-length",  required_argument, 0, 0},
    {"frame-start",   required_argument, 0, 0},
    {"frame-srate",   required_argument, 0, 0},
    {"seglen",        required_argument, 0, 0},
    {"srate",         required_argument, 0, 0},
    {"segment-start", required_argument, 0, 0},
    {"trigtime",      required_argument, 0, 0},
    {"median",        no_argument, 0, 0},
    {"help",          no_argument, 0,'h'},
    {0, 0, 0, 0}
  };

  int opt=0;
  int long_index=0;

  int argCount = 0;
  int argcheck[REQARG];
  for(i=0; i<REQARG; i++) argcheck[i] = 0;

  //Loop through argv string and pluck out arguments
  while ((opt = getopt_long_only(argc, argv,"apl:b:", long_options, &long_index )) != -1)
  {
    switch (opt)
    {

      case 0:
        if(strcmp("ifo",           long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->ifo, "%s", optarg);
        }
        if(strcmp("glitch-model",  long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->bw_model, "%s", optarg);
        }
        if(strcmp("cachefile", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_cache, "%s", optarg);
        }
        if(strcmp("channel", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_chanl, "%s", optarg);
        }
        if(strcmp("frame-type", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_type, "%s", optarg);
        }
        if(strcmp("frame-length", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_seglen = (double)atof(optarg);
        }
        if(strcmp("frame-start", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_start = (double)atof(optarg);
        }
        if(strcmp("frame-srate", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_srate = (double)atof(optarg);
        }
        if(strcmp("seglen", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_seglen = (double)atof(optarg);
        }
        if(strcmp("srate", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_srate = (double)atof(optarg);
        }
        if(strcmp("segment-start", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_start = (double)atof(optarg);
        }
        if(strcmp("trigtime", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_trigtime = (double)atof(optarg);
        }
        if(strcmp("median",long_options[long_index].name) == 0)
        {
          data->medianFlag = 1;
        }
        if(strcmp("help", long_options[long_index].name) == 0)
        {
          fprintf(stdout,"BayesWaveGlitchFrame:\n");
          fprintf(stdout,"  Create glitch-subtracted frames from BayesWave residual\n");
          fprintf(stdout,"\n");
          print_usage();
          exit(0);
        }
        break;
      case 'h':
        fprintf(stdout,"BayesWaveGlitchFrame:\n");
        fprintf(stdout,"  Create glitch-subtracted frames from BayesWave residual\n");
        fprintf(stdout,"\n");
        print_usage();
        exit(0);
        break;
      default: print_usage();
        exit(0);
    }
  }

  //make sure there all the right arguments were used
  for(i=0; i<REQARG; i++)
  {
    if(argcheck[i]!=1)
    {
      fprintf(stdout,"\nbayeswave_cleanframe: missing requird argument\n\n");
      print_usage();
      exit(0);
    }
  }

  //See how we did with th command line
  fprintf(stdout,"interferometer.............%s\n",data->ifo);
  fprintf(stdout,"\n");
  fprintf(stdout,"cache file.................%s\n",data->fr_cache);
  fprintf(stdout,"channel type...............%s\n",data->fr_chanl);
  fprintf(stdout,"frame type.................%s\n",data->fr_type);
  fprintf(stdout,"\n");
  fprintf(stdout,"frame segment length.......%li\n",(long)data->fr_seglen);
  fprintf(stdout,"frame start time...........%li\n",(long)data->fr_start);
  fprintf(stdout,"frame sampling rate........%li\n",(long)data->fr_srate);
  fprintf(stdout,"\n");
  fprintf(stdout,"bayeswave glitch model.....%s\n",data->bw_model);
  fprintf(stdout,"bayeswave segment length...%li\n",(long)data->bw_seglen);
  fprintf(stdout,"bayeswave start time.......%li\n",(long)data->bw_start);
  fprintf(stdout,"bayeswave sampling rate....%li\n",(long)data->bw_srate);
  fprintf(stdout,"bayeswave trigger time.....%li\n",(long)data->bw_trigtime);
  fprintf(stdout,"\n");
}

void print_usage()
{
  fprintf(stdout,"Requird Arguments:\n");
  fprintf(stdout,"--ifo           interferometers (H1,L1,..)\n");
  fprintf(stdout,"--glitch-model  glitch reconstruction\n");
  fprintf(stdout,"--cachefile     cache files pointing to original frames\n");
  fprintf(stdout,"--channel       data channel name in original frames\n");
  fprintf(stdout,"--frame-type    original frame type\n");
  fprintf(stdout,"--frame-length  frame length\n");
  fprintf(stdout,"--frame-start   frame start time\n");
  fprintf(stdout,"--frame-srate   frame sammpling rate\n");
  fprintf(stdout,"--seglen        bayeswave segment length\n");
  fprintf(stdout,"--srate         bayeswave sampling rate\n");
  fprintf(stdout,"--segment-start bayeswave segment start time\n");
  fprintf(stdout,"--trigtime      bayeswave trigger time\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"Optional Arguments:\n");
  fprintf(stdout,"--median        use median glitch reconstruction\n");
  fprintf(stdout,"                can only be used on a completed run\n");
  fprintf(stdout,"--help          print help output\n");
  fprintf(stdout,"\n");
}

